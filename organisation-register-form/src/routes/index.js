import VueRouter from "vue-router";
import Vue from "vue";
import PersonalDetailsPage from "../components/PersonalDetailsPage";
import CompanyDetailsPage from "../components/CompanyDetailsPage";
import OtpPage from "../components/OtpPage";
import SuccessPage from "../components/SuccessPage"
// import { AuthGuard}  from "../services/Authentication";


Vue.use(VueRouter);

const routes = [
    {
        path:"/",
        name:"PersonalDetailsPage",
        component: PersonalDetailsPage
    },
    {
        path:"/company-details",
        name:"CompanyDetailsPage",
        component: CompanyDetailsPage,
    },
    {
        path:"/otp-page",
        name:"OtpPage",
        component: OtpPage,
    },
    {
        path:"/success-page",
        name:"SuccessPage",
        component: SuccessPage
    },
    
]

export default new VueRouter({
    routes
});