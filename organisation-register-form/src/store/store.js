import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        personalDetails : {
            full_name:""
        },
        companyDetails : {},
        otpValue : ""
    },
    getters: {
            getPersonalDetails : state => state.personalDetails,
            getCompanyDetails : state => state.companyDetails,
            getOtpDetails : state => state.otpValue
    },
    mutations: {
        addPersonalDetailsList: (state,details) =>{
            state.personalDetails = details
        },
        addCompanyDetailsList: (state,details) =>{
            state.companyDetails = details
        },
        addOtpValueMutation: (state,value) =>{
            state.otpValue = value
        },
        
    },
    actions: {
        addPersonalDetails: (context,data) =>{
           context.commit("addPersonalDetailsList",data);
        },
        addCompanyDetails : (context,data) =>{
            context.commit("addCompanyDetailsList",data);
        },
        addOtpValue : (context,data) =>{
            context.commit("addOtpValueMutation",data);
        }
    }

})