import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
//Router
import router from "./routes"
//Store
import {store} from "./store/store";
import OtpInput from "@bachdgvn/vue-otp-input";
import VueTelInput from 'vue-tel-input'
 
Vue.use(VueTelInput)
 
Vue.component("v-otp-input", OtpInput);

Vue.use(Vuelidate)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
